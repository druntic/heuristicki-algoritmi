import random

text_file = open("rjesenjaGreedy.txt", "w")
slova=['B','C','D','E']
for slovo in slova:
    if slovo=='B' or slovo=='C':
        to=2
    else:
        to=11
    for b in range(1,to):
        for a in range(1,11):
            with open('testCases\primjeri'+chr(92)+slovo+str(b)+'.'+str(a)) as f:
                lines = f.readlines()
            with open('testCases\opt'+chr(92)+slovo+str(b)+'.'+str(a)+'.opt') as f:
                lines3 = f.readlines()
            #ukupno n+2 redaka
            #redovi nakon n=2 su oblika [redni broj][trosak otvaranja skladista j][trosak povezivanja grada j sa skladistem i]
            #j=1,..,m i=1,..,n
            #jedno skladiste moze biti povezano s vise gradova
            
            n=int(lines[1].split()[0])#broj potencijalnih skladista
            m=int(lines[1].split()[1])#broj gradova
            solution=[0]*(m+1)
            lines2=[0]*n
            for i in range(2,n+2):
                lines2[i-2]=[int(a) for a in lines[i].split()]

            
            #ideja: uzmi trosak otvaranja, sumiraj sa svakim troskom povezaivanja i odaberi najmanji izmedu njih
            #trosak otvaranja po skladistu se sumira samo jednom
            
            
            #total cost
            def getlist():
                list1=[0]*n
                for i in range(0,n):
                    #print(i)
                    list2=[0]*m
                    for j in range(2,m+2):
                        
                        list2[j-2]=int(lines2[i][1])+int(lines2[i][j])
                    list1[i]=list2
                return list1
            
            list1 = getlist()

            #best
            
            r = list(range(m))#odaberi random grad za pocetak
            random.shuffle(r)
                
            min=list1[0][0]
            jj=1
            sum=0
            for i in r:
                min=list1[0][i]
                for j in range(n):
                    if min>list1[j][i]:
                        min=list1[j][i]
                        jj=j+1
                solution[i]=jj
                lines2[jj-1][1]=0
                list1 = getlist()
                
                #print(min)
                sum+=min
                
            
            solution[m]=sum
#string=slovo+str(b)+'.'+str(a)+ ' ' + ' '.join([str(elem) for elem in solution])
#print(string)
            string=slovo+str(b)+'.'+str(a)+ ' ' + ' '.join([str(elem) for elem in solution])
            #print(string)
            #print(lines2[0].split()[-1])
            if int(lines3[0].split()[-1])==sum:
                text_file.write(string+' DA'+'\n')
            else:
                text_file.write(string+' NE'+'\n')
                
            
        
text_file.close()
        
        
        
        

        
       
                   
                   
        


        
    
        
    
