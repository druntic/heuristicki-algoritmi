
import random
import copy

text_file1 = open("rjesenjaLS1.txt", "w")
text_file2 = open("rjesenjaLS2.txt", "w")
slova=['B','C','D','E']
itr=0
for slovo in slova:
    if slovo=='B' or slovo=='C':
        to=2
    else:
        to=11
    for b in range(1,to):
        for a in range(1,11):
            with open('testCases\primjeri'+chr(92)+slovo+str(b)+'.'+str(a)) as f:
                lines = f.readlines()
            with open('testCases\opt'+chr(92)+slovo+str(b)+'.'+str(a)+'.opt') as f:
                lines3 = f.readlines()
            with open('rjesenjaGreedy.txt') as f:
                rjesenjaGreedy = f.readlines()

            print(slovo+str(b)+'.'+str(a))
            
            #ukupno n+2 redaka
            #redovi nakon n=2 su oblika [redni broj][trosak otvaranja skladista j][trosak povezivanja grada j sa skladistem i]
            #j=1,..,m i=1,..,n
            #jedno skladiste moze biti povezano s vise gradova
            
            n=int(lines[1].split()[0])#broj potencijalnih skladista
            m=int(lines[1].split()[1])#broj gradova
            solution=[0]*(m+1)
            lines2=[0]*n
            for i in range(2,n+2):
                lines2[i-2]=[int(a) for a in lines[i].split()]
            #print(lines2)
            
            def Convert(string):#izbaci prvi i zadnji element i pretvori string u listu integera
                li = list(string.split(" "))[1:][:-1]
                li = [int(i) for i in li]
                return li
            #print(rjesenjaGreedy[itr])
            sol=Convert(rjesenjaGreedy[itr])
            
            def ConvertToBinary(sol):#ako je n-to skladiste dio rjesenja onda je n-ti element 1, inace 0
                n=int(lines[1].split()[0])
                list=[0]*n
                for i in range(1,n+1):
                    if i in sol:
                        list[i-1]=1
                return list
            
            def GetAllNeighbors(binarysol):
                list1=[0]*len(binarysol)
                for i in range(len(binarysol)):
                    z=binarysol[i]
                    if binarysol[i]==0:
            
                        binarysol[i]=1
                    else:
                        binarysol[i]=0
                    
                    list1[i]=binarysol.copy()
                    binarysol[i]=z
            
                return list1
            
            def GetAllNeighbors2(binarysol):
                list1=[0]*len(binarysol)
                for i in range(len(binarysol)):
                    z=binarysol[i]
                    if binarysol[i]==0:
            
                        binarysol[i-1]=1
                        binarysol[i+1]=1
                    else:
                        binarysol[i-1]=0
                        binarysol[i+1]=0
                    
                    list1[i]=binarysol.copy()
                    binarysol[i]=z
            
                return list1
            
            
            def getlist(lines2):
                list1=[0]*n
                for i in range(0,n):
                    #print(i)
                    list2=[0]*m
                    for j in range(2,m+2):
                        
                        list2[j-2]=int(lines2[i][1])+int(lines2[i][j])
                    list1[i]=list2
                return list1
            
            def GetNewSolution(neighbor, lines):
                z = copy.deepcopy(lines)
                suma=0
                for i in range(len(neighbor)-1):
                    if neighbor[i]==1:
                        #print(z[i+1])
                        suma+=z[i+1][1]
                        #print(suma)
                        z[i+1][1]=0
            
                list1 = getlist(z)
                #best
                
                r = list(range(m))#odaberi random grad za pocetak
                random.shuffle(r)
                    
                jj=1
                for i in r:
                    min=list1[0][i]
                    for j in range(n):
                        if min>list1[j][i]:
                            min=list1[j][i]
                            jj=j+1
                    solution[i]=jj-1
                    z[jj-1][1]=0
                    list1 = getlist(z)
                    
                    #print(min)
                    suma+=min
                solution[m]=suma
                return solution
                
            
            x=ConvertToBinary(sol)
            
            #najbolji poboljsavajuci susjed
            for a in range(11):
                
                temp=x
                #print(temp)
                for i in GetAllNeighbors(x):
                    z=GetNewSolution(temp, lines2)[-1]
                    if z>GetNewSolution(i, lines2)[-1]:
                        temp=ConvertToBinary(GetNewSolution(i, lines2))
                x=temp
                #print(GetNewSolution(x, lines2)[-1])
            
            sol1=GetNewSolution(x, lines2)
            
            
            #prvi poboljsavajuci susjed
            for a in range(10):#doci ce do optimalnog ako povecamo broj iteracija, stavio sam 10 jer je presporo inace
            
                temp=x
                #print(temp)
                for i in GetAllNeighbors(x):
                    z=GetNewSolution(temp, lines2)[-1]
                    if z>GetNewSolution(i, lines2)[-1]:
                        temp=ConvertToBinary(GetNewSolution(i, lines2))
                        break
                x=temp
                sol2=GetNewSolution(x, lines2)
                
            string=slovo+str(b)+'.'+str(a)+ ' ' + ' '.join([str(elem) for elem in solution])
            #print(string)
            #print(lines2[0].split()[-1])
            if int(lines3[0].split()[-1])==sol1[-1]:
                text_file1.write(string+' DA'+'\n')
                print("Da")
            else:
                text_file1.write(string+' NE'+'\n')
                print("Ne", lines3[0].split()[-1],"!=", sol1[-1])
            
            if int(lines3[0].split()[-1])==sol2[-1]:
                text_file2.write(string+' DA'+'\n')
            else:
                text_file2.write(string+' NE'+'\n')


text_file1.close()
text_file2.close()














        
    