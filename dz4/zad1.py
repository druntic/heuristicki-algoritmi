
import random
import copy
import numpy as np
text_file1 = open("GA generacijski.txt", "w")

slova=['B','C','D','E']
itr=0
optimalnih=0
for slovo in slova:
    if slovo=='B' or slovo=='C':
        to=2
    else:
        to=11
    for b in range(1,to):
        for a1 in range(1,11):
            with open('testCases\primjeri'+chr(92)+slovo+str(b)+'.'+str(a1)) as f:
                lines = f.readlines()

            with open('testCases\opt'+chr(92)+slovo+str(b)+'.'+str(a1)+'.opt') as f:
                lines3 = f.readlines()
            with open('rjesenjaGreedy.txt') as f:
                rjesenjaGreedy = f.readlines()

            print(slovo+str(b)+'.'+str(a1))
            
            #ukupno n+2 redaka
            #redovi nakon n=2 su oblika [redni broj][trosak otvaranja skladista j][trosak povezivanja grada j sa skladistem i]
            #j=1,..,m i=1,..,n
            #jedno skladiste moze biti povezano s vise gradova
            
            n=int(lines[1].split()[0])#broj potencijalnih skladista
            m=int(lines[1].split()[1])#broj gradova
            solution=[0]*(m+1)
            lines2=[0]*n
            for i in range(2,n+2):
                lines2[i-2]=[int(a) for a in lines[i].split()]
            #print(lines2)
            
            def Convert(string):#izbaci prvi i zadnji element i pretvori string u listu integera
                li = list(string.split(" "))[1:][:-1]
                li = [int(i) for i in li]
                return li
            #print(rjesenjaGreedy[itr])
            sol=Convert(rjesenjaGreedy[itr])
            itr+=1
            
            def ConvertToBinary(sol):#ako je n-to skladiste dio rjesenja onda je n-ti element 1, inace 0
                n=int(lines[1].split()[0])
                list=[0]*n
                for i in range(1,n+1):
                    if i in sol:
                        list[i-1]=1
                return list
            

            
            def getlist(lines2):
                list1=[0]*n
                for i in range(0,n):
                    #print(i)
                    list2=[0]*m
                    for j in range(2,m+2):
                        
                        list2[j-2]=int(lines2[i][1])+int(lines2[i][j])
                    list1[i]=list2
                return list1
            
            def GetNewSolution(neighbor, lines):
                z = copy.deepcopy(lines)
                suma=0
                for i in range(len(neighbor)-1):
                    if neighbor[i]==1:
                        #print(z[i+1])
                        suma+=z[i+1][1]
                        #print(suma)
                        z[i+1][1]=0
            
                list1 = getlist(z)
                #best
                
                r = list(range(m))#odaberi random grad za pocetak
                random.shuffle(r)
                    
                jj=1
                blacklist=[]
                for i in r:
                    min=list1[0][i]
                    for j in range(n):
                        if min>list1[j][i]:
                            min=list1[j][i]
                            jj=j+1
                    solution[i]=jj-1
                    #z[jj-1][1]=0
                    #list1 = getlist(z)
                    #print(jj)
                    if jj not in blacklist:
                        for i in range(len(list1[1])):
                            list1[jj-1][i]-=z[jj-1][1]
                    blacklist.append(jj)
                    
                    #print(min)
                    suma+=min
                solution[m]=suma
                return solution
            
            def RandomSolutions(n1):
                list2=[]
                for j in range(n1):
                    list1=[]
                    for i in range(n):
                        list1.append(random.randint(0, 1))
                    list2.append(list1)
                return list2
                    
            def roulette_wheel_selection(population):
                indeksi=list(range(0, len(population)))

                population_fitness = sum([1/GetNewSolution(x, lines2)[-1] for x in population])

                chromosome_probabilities = [(1/GetNewSolution(x, lines2)[-1])/population_fitness for x in population]
                chromosome_probabilities= np.array(chromosome_probabilities)
                chromosome_probabilities /= sum(chromosome_probabilities)#normalizacija
                if round(sum(chromosome_probabilities),5)!=1:
                    for x in population:
                        print("value:",GetNewSolution(x, lines2)[-1])
                        print("inverse:",1/GetNewSolution(x, lines2)[-1])
                    print("sum:",population_fitness)
                    text_file1.close()
                    roulette_wheel_selection(population)
                indeks=np.random.choice(indeksi, p=chromosome_probabilities)
                return population[indeks]
            def tournamentSelection(population):
                k=2
                kpopulation=random.sample(population, k)
                best=kpopulation[0]
                for x in kpopulation:
                    if GetNewSolution(x, lines2)<GetNewSolution(best, lines2):
                        best=x
                return best
                
            
            def crossParents(parent1,parent2):
                n=random.randint(1, len(parent1)-1)#
                left1=parent1[:n]
                right1=parent1[n:]
                left2=parent2[:n]
                right2=parent2[n:]
                if (GetNewSolution(left1+right2, lines2)[-1]<GetNewSolution(left2+right1, lines2)[-1]):
                    return left1+right2
                return left2+right1
            def mutateChild(child):
                n=random.randint(0, len(child)-1)#
                if child[n]==1:
                    child[n]=0
                else:
                    child[n]=1
                return child
                
            
            def G(): 
                parents=RandomSolutions(5)  
                best=parents[0]
                secondbest=[parents[1]]#pamtimo 2 najbolja
                #print("best:",GetNewSolution(roulette_wheel_selection(parents), lines2)[-1])
                for i in range(150):
                    for x in parents:
                        if (GetNewSolution(x, lines2)[-1]<GetNewSolution(best, lines2)[-1]):
                            secondbest=best
                            best=x
                    children=[]
                    children.append(best)
                    #children.append(secondbest)
                    while(len(children)<len(parents)):
                        parent1=tournamentSelection(parents)
                        parent2=tournamentSelection(parents)
                        if parent1!=parent2:
                            child=crossParents(parent1, parent2)
                            child=mutateChild(child)
                            #if(GetNewSolution(worst, lines2)[-1]>GetNewSolution(child, lines2)[-1]):
                            children.append(child)
                    parents=children
                return GetNewSolution(best, lines2)
                
            
            
            def EL():
                parents=RandomSolutions(20)  
                
                #print("best:",GetNewSolution(roulette_wheel_selection(parents), lines2)[-1])
                for i in range(100):
                    worst=parents[0]
                    for x in parents:
                        if (GetNewSolution(x, lines2)[-1]>GetNewSolution(worst, lines2)[-1]):
                            worst=x
                    
                    children=[]
                    #children.append(best)
                    #children.append(secondbest)
                    
                    children=copy.deepcopy(parents)
                    children.remove(worst)
                    while(len(children)<len(parents)):
                        parent1=roulette_wheel_selection(parents)
                        parent2=roulette_wheel_selection(parents)
                        if parent1!=parent2:
                            child=crossParents(parent1, parent2)
                            child=mutateChild(child)
                            if(GetNewSolution(worst, lines2)[-1]>GetNewSolution(child, lines2)[-1]):
                                children.append(child)
                    #print(GetNewSolution(best, lines2)[-1])
                
                    parents=children

                best=parents[0]
                if (GetNewSolution(x, lines2)[-1]<GetNewSolution(best, lines2)[-1]):
                    best=x
                return GetNewSolution(best, lines2)
            sol1=EL()
            
            
                
            string=slovo+str(b)+'.'+str(a1)+ ' ' + ' '.join([str(elem) for elem in solution])
            #print(string)
            #print(lines2[0].split()[-1])
            if int(lines3[0].split()[-1])==sol1[-1]:
                text_file1.write(string+' DA'+'\n')
                print("Da")
                optimalnih+=1
            else:
                text_file1.write(string+' NE'+'\n')
                print("Ne", lines3[0].split()[-1],"!=", sol1[-1])
            print("optimalnih: ", optimalnih,"/", itr )

text_file1.close()















        
    