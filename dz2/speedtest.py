import random
import copy
import cProfile
import pstats


with open('testCases\primjeri\primjer.neki.py') as f:
    lines = f.readlines()


n=int(lines[1].split()[0])#broj potencijalnih skladista
m=int(lines[1].split()[1])#broj gradova
solution=[0]*(m+1)
neighbor=[12]*m

lines2=[0]*n
for i in range(2,n+2):
    lines2[i-2]=[int(a) for a in lines[i].split()]

def getlist(lines2):
    list1=[0]*n
    for i in range(0,n):
        #print(i)
        list2=[0]*m
        for j in range(2,m+2):
            #print("test:",lines2[i])
            list2[j-2]=int(lines2[i][1])+int(lines2[i][j])
        list1[i]=list2
    return list1
            
def GetNewSolution():
    
    z = copy.deepcopy(lines2)
    suma=0
    for i in range(len(neighbor)-1):
        if neighbor[i]==1:
            #print(z[i+1])
            suma+=z[i+1][1]
            #print(suma)
            z[i+1][1]=0

    list1 = getlist(z)
    #best
    
    r = list(range(m))#odaberi random grad za pocetak
    random.shuffle(r)
        
    jj=1
    for i in r:
        min=list1[0][i]
        for j in range(n):
            if min>list1[j][i]:
                min=list1[j][i]
                jj=j+1
        solution[i]=jj-1
        #z[jj-1][1]=0
        #list1 = getlist(z)
        for i in range(len(list1[1])):
            list1[jj-1][i]-=z[jj-1][1]
        
        #print(min)
        suma+=min
    solution[m]=suma
    return solution

profile = cProfile.Profile()

profile.runcall(GetNewSolution)
ps = pstats.Stats(profile)
ps.sort_stats(pstats.SortKey.TIME)
ps.print_stats()

# for x in lines2:
#     print(x)
# for x in getlist(lines2):
#     print(x)
