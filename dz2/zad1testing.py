
import random
import copy

text_file1 = open("simulatedAnnealing.txt", "w")

slova=['B']
itr=0
optimalnih=0
for slovo in slova:
    if slovo=='B' or slovo=='C':
        to=2
    else:
        to=11
    for b in range(1,to):
        for a1 in range(1,2):
            with open('testCases\primjeri'+chr(92)+slovo+str(b)+'.'+str(a1)) as f:
                lines = f.readlines()

            with open('testCases\opt'+chr(92)+slovo+str(b)+'.'+str(a1)+'.opt') as f:
                lines3 = f.readlines()
            with open('rjesenjaGreedy.txt') as f:
                rjesenjaGreedy = f.readlines()

            print(slovo+str(b)+'.'+str(a1))
            
            #ukupno n+2 redaka
            #redovi nakon n=2 su oblika [redni broj][trosak otvaranja skladista j][trosak povezivanja grada j sa skladistem i]
            #j=1,..,m i=1,..,n
            #jedno skladiste moze biti povezano s vise gradova
            
            n=int(lines[1].split()[0])#broj potencijalnih skladista
            m=int(lines[1].split()[1])#broj gradova
            solution=[0]*(m+1)
            lines2=[0]*n
            for i in range(2,n+2):
                lines2[i-2]=[int(a) for a in lines[i].split()]
            #print(lines2)
            
            def Convert(string):#izbaci prvi i zadnji element i pretvori string u listu integera
                li = list(string.split(" "))[1:][:-1]
                li = [int(i) for i in li]
                return li
            #print(rjesenjaGreedy[itr])
            sol=Convert(rjesenjaGreedy[itr])
            itr+=1
            
            def ConvertToBinary(sol):#ako je n-to skladiste dio rjesenja onda je n-ti element 1, inace 0
                n=int(lines[1].split()[0])
                list=[0]*n
                for i in range(1,n+1):
                    if i in sol:
                        list[i-1]=1
                return list
            
            
            def flip(p):
                return True if random.random() < p else False
            


            
            def getlist(lines2):
                list1=[0]*n
                for i in range(0,n):
                    #print(i)
                    list2=[0]*m
                    for j in range(2,m+2):
                        
                        list2[j-2]=int(lines2[i][1])+int(lines2[i][j])
                    list1[i]=list2
                return list1
            
            
            def buildSolution(lines, partialSolution):#vrati najbolje rj za dana skladista
                z = copy.deepcopy(lines)
                suma=0
                solution=[]
                for i in partialSolution:
                    
                    suma+=z[i-1][1]
                    z[i-1][1]=0
                    #print(suma)
                
                list1 = getlist(z)
                #print(list1[0])
                for j in range(m):
                    minn=list1[0][j]
                    k=0
                    
                    for i in range(n):
                        
                        if minn>list1[i][j]:
                            minn=list1[i][j]
                            k=i
                    solution.append(k+1)
                    
                    suma+=list1[k][j]
                solution.append(suma)
                return solution
                    
            print(buildSolution(lines2,[13,12]))
            
            def aco(m, ro, alpha,iter, tau):
                

                    
    
    
                    
            # string=slovo+str(b)+'.'+str(a1)+ ' ' + ' '.join([str(elem) for elem in solution])
            # #print(string)
            # #print(lines2[0].split()[-1])
            # if int(lines3[0].split()[-1])==sol1[-1]:
            #     text_file1.write(string+' DA'+'\n')
            #     print("Da")
            #     optimalnih+=1
            # else:
            #     text_file1.write(string+' NE'+'\n')
            #     print("Ne", lines3[0].split()[-1],"!=", sol1[-1])
            # print("optimalnih: ", optimalnih,"/", itr )

text_file1.close()















        
    